# Реализовать функцию my_func(), которая принимает три позиционных аргумента,
# и возвращает сумму наибольших двух аргументов.

def my_func(num1, num2, num3):
    num_list = [num1, num2, num3]
    max_num = max(num_list)
    num_list.remove(max_num)
    if num_list[0] > num_list[1]:
        result = max_num + num_list[0]
    else:
        result = max_num + num_list[1]
    print(result)


while True:
    try:
        my_func(num1=int(input("First number:\n")),
                num2=int(input("Second number:\n")),
                num3=int(input("Third number:\n")))
        break
    except ValueError:
        print("Invalid argument. Numbers expected")
