
def sum_func():
    final_result = 0
    terminate = False

    while not terminate:
        numbers = input('Please, enter numbers separated by spaces or Q for quit:').split()
        current_result = 0
        for index in range(len(numbers)):
            if numbers[index] == "q" or numbers[index] == "Q":
                terminate = True
                break
            else:
                try:
                    current_result += float(numbers[index])
                except ValueError:
                    print("Invalid arguments. Numbers expected")
        print(f"Current result: {current_result}")
        final_result += current_result
    print(f"Final result: {final_result}")


sum_func()
