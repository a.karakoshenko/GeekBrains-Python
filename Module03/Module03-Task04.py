# Программа принимает действительное положительное число x и целое отрицательное число y.
# Необходимо выполнить возведение числа x в степень y.
# Задание необходимо реализовать в виде функции my_func(x, y).
# При решении задания необходимо обойтись без встроенной функции возведения числа в степень.

def power(x, y):
    if x == 0:
        print("Undefined")
    else:
        return 1 / (x ** abs(y))


def power_loop(x, y):
    if x == 0:
        print("Undefined")
    else:
        z = 1
        for i in range(abs(y)):
            z *= x
        return 1 / z


print("Powering without looping")
try:
    print(power(float(input("Enter X:\n")), int(input("Enter Y: \n"))))
except ValueError:
    print("Invalid arguments. Numbers expected")

print("Powering through for-loop")
try:
    print(power_loop(float(input("Enter X:\n")), int(input("Enter Y:\n"))))
except ValueError:
    print("Invalid arguments. Numbers expected")
