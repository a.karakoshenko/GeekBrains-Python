# Реализовать функцию int_func(), принимающую слово из маленьких латинских букв и возвращающую его же,
# но с прописной первой буквой. Например, print(int_func(‘text’)) -> Text.

def int_func(words):
    sentence = words.split(" ")
    result = []
    for word in sentence:
        word = word.title()
        result.append(word)
    return result


string = input("Please, type some text and press enter:\n")
print(*int_func(string))
