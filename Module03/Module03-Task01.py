# Реализовать функцию, принимающую два числа (позиционные аргументы) и выполняющую их деление.
# Числа запрашивать у пользователя, предусмотреть обработку ситуации деления на ноль.

def division(*args):
    while True:
        try:
            number1 = input("Enter first number:\n")
            number2 = input("Enter second number:\n")
            result = float(number1) / float(number2)
        except ZeroDivisionError:
            print("You can't divide by zero")
        except ValueError:
            print("Invalid argument. Numbers expected")
        else:
            return result
            break


print(f"Result: {division()}")
