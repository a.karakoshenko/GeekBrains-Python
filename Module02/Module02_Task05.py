# Реализовать структуру «Рейтинг», представляющую собой не возрастающий набор натуральных чисел.
# У пользователя необходимо запрашивать новый элемент рейтинга.
# Если в рейтинге существуют элементы с одинаковыми значениями,
# то новый элемент с тем же значением должен разместиться после них.

rating_list = [10, 7, 5, 3, 2]

while True:
    rating_entry = input("Please enter a new rating number:\n")
    if rating_entry.isdigit():
        rating_entry = int(rating_entry)
        for entry in rating_list:
            if rating_list.count(rating_entry) > 0:
                rating_list.insert(rating_list.index(rating_entry) + rating_list.count(rating_entry), rating_entry)
                break
            else:
                if rating_entry > entry:
                    rating_list.insert(rating_list.index(entry), rating_entry)
                    break
                else:
                    rating_list.append(rating_entry)
                    break
        break
    print("Invalid entry! Please, enter a number!")
print(rating_list)

