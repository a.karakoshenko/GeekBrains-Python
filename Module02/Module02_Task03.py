# Пользователь вводит месяц в виде целого числа от 1 до 12.
# Сообщить к какому времени года относится месяц (зима, весна, лето, осень).
# Напишите решения через list и через dict.

seasons_list = ("winter", "spring", "summer", "autumn")
seasons_dict = {0: "winter", 1: "spring", 2: "summer", 3: "autumn"}

while True:
    month_index = input("Please, enter a month number [1..12]:\n")
    if month_index.isdigit() and 1 <= int(month_index) <= 12:
        season_index = int(month_index) % 12 // 3
        break
    print("Invalid month number! Please, enter a number from 1 to 12.")

print("The season is: ", seasons_list[season_index])
print("The season is: ", seasons_dict[season_index])
