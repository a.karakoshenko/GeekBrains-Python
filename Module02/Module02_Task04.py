# Пользователь вводит строку из нескольких слов, разделённых пробелами.
# Вывести каждое слово с новой строки. Строки необходимо пронумеровать.
# Если в слово длинное, выводить только первые 10 букв в слове.

word_list = [word for word in input("Please, enter some words separated by spaces:\n").split()]
print(word_list)
for word in word_list:
    print(word_list.index(word) + 1, word[:10])
