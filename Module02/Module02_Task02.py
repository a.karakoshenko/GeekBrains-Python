
# Для списка реализовать обмен значений соседних элементов, т.е.
# Значениями обмениваются элементы с индексами 0 и 1, 2 и 3 и т.д.
# При нечетном количестве элементов последний сохранить на своем месте.
# Для заполнения списка элементов необходимо использовать функцию input().

my_list = [element for element in input("Please, enter list items separated by spaces:").split()]
print(my_list)
for index in range(1, len(my_list), 2):
    my_list[index - 1], my_list[index] = my_list[index], my_list[index - 1]
print(my_list)
