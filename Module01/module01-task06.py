# Спортсмен занимается ежедневными пробежками. В первый день его результат составил a километров.
# Каждый день спортсмен увеличивал результат на 10 % относительно предыдущего.
# Требуется определить номер дня, на который результат спортсмена составит не менее b километров.
# Программа должна принимать значения параметров a и b и выводить одно натуральное число — номер дня.

run_distance = float(input("Enter your current run distance:"))
desired_distance = float(input("Enter your desired run distance:"))
days = 1
while run_distance < desired_distance:
    run_distance = run_distance + (run_distance * 0.1)
    days += 1
print("Your run goal can be achieved on day:", days)

