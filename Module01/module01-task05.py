# Запросите у пользователя значения выручки и издержек фирмы.
# Определите, с каким финансовым результатом работает фирма
# (прибыль — выручка больше издержек, или убыток — издержки больше выручки).
# Выведите соответствующее сообщение. Если фирма отработала с прибылью,
# вычислите рентабельность выручки (соотношение прибыли к выручке).
# Далее запросите численность сотрудников фирмы и определите прибыль
# фирмы в расчете на одного сотрудника.

income = float(input("Company income, USD:"))
costs = float(input("Company costs, USD:"))
fin_result = income - costs
if income > costs:
    print("Your company income:", "{:.2f}".format(fin_result), "USD")
    print("Your company profitability:", "{:.2f}".format(fin_result / income), "USD")
    employees = int(input("Company employees:"))
    print("Income per employee:", "{:.2f}".format(fin_result / employees), "USD")
else:
    print("Your company losses:", abs(fin_result), "USD")
